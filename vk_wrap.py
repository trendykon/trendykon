import logging
from math import floor
import vk_api

api = vk_api.VkApi(api_version=5.45)


BASE_URL = "https://api.vk.com/method/"
COMMUNITY_ID = -76982440

logger = logging.getLogger(__name__)

logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                              datefmt="%H:%M:%S")

sh = logging.StreamHandler()
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)

logger.addHandler(sh)


def get_posts_by_100(id, count, domain='', offset=0, filter='all', extendend=False, fields=None):

    for i in range(floor(count / 100) + 1):
        logger.info("Retrieving posts {}-{} from {}".format(i * 100,
                                                            min(i * 100 + 100, count),
                                                            count))
        posts = api.method('wall.get',
                           {'owner_id': id,
                            'domain': domain,
                            'offset': offset + i * 100,
                            'count': min(100, count - i * 100),
                            'filter': filter,
                            'extendend': int(extendend),
                            'fields': fields})

        yield posts['items']


def get_first_level_reposts(post, offset=0):
    """

    :param post: Словарь, содержит как минимум owner_id и post_id
    :return:
    """

    first_part = api.method('wall.getReposts',
                            {
                                'owner_id': post.get('owner_id', post['from_id']),
                                'post_id': post['id'],
                                'count': 1000,
                                'offset': offset
                            })['items']
    second_part = api.method('wall.getReposts',
                            {
                                'owner_id': post.get('owner_id', post['from_id']),
                                'post_id': post['id'],
                                'count': 1000,
                                'offset': offset + len(first_part)
                            })['items']
    return first_part + second_part

def get_all_reposts(post, level):


    result = []
    if post.get('reposts', {'count': 0})['count'] == 0:
        return [{**post, 'level': level}]

    logger.debug('Making request...')
    reposts = get_first_level_reposts(post)
    logger.debug('Done')

    for repost in reposts:
        result.extend(get_all_reposts(repost, level + 1))

    return result


if __name__ == '__main__':
    post = api.method('wall.getById', {'posts': '-29534144_3308685'})[0]
    reposts = get_all_reposts(post, 0)
    print(len(reposts))
    print(len(list(filter(lambda x: x['level']==2, reposts))))
    print(len(list(filter(lambda x: x['level']==3, reposts))))
    print()
    for r in reposts:
        print(r['level'])
